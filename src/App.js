import { useEffect, useState } from "react";
import "./App.css";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
import SearchBar from "./components/SearchBar";
// import book from "./Data.json";
function App() {
  const [data, setData] = useState([]);
  useEffect(() => {
    const url = "https://titan-prod-new.herokuapp.com/searchingoogle";
    fetch(url, {
      method: "POST",
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
      body: JSON.stringify({
        text: "origin",
      }),
    })
      .then((res) => res.json())
      .then((books) => setData(books.matchingBooks));
  }, []);
  // console.log(data);
  return (
    <div className="App">
      <SearchBar placeholder="Enter a Book Name..." data={data} />
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored"
      />
    </div>
  );
}

export default App;
